package ru.itis.repositories;

import ru.itis.models.Product;

import java.util.List;

public interface ProductsRepository {
    void save(Product product);
    List<Product> findAll();
    List<Product> findProductsLikeName(String name);
    List<Product> findAllOrderByIdDesc();
    List<Product> findAllOrderByNameDesc();
    List<Product> findAllOrderByCountDesc();

}
