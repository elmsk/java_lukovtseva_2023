package ru.itis.services.impl;

import lombok.RequiredArgsConstructor;

import org.springframework.stereotype.Service;
import ru.itis.dto.ProductDto;
import ru.itis.models.Product;
import ru.itis.repositories.ProductsRepository;
import ru.itis.services.ProductsService;
import java.util.List;

import static ru.itis.dto.ProductDto.from;
@RequiredArgsConstructor
@Service
public class ProductsServiceImpl implements ProductsService {

    private final ProductsRepository productsRepository;

    @Override
    public List<ProductDto> signUp(ProductDto productData) {
            Product product = Product.builder()
                    .name(productData.getName())
                    .category(productData.getCategory())
                    .count(productData.getCount())
                    .build();
            productsRepository.save(product);
            return from(productsRepository.findAll());
    }
    @Override
    public List<ProductDto> listOfProducts() {
        return from(productsRepository.findAll());
    }
    @Override
    public List<ProductDto> getProductLikeName(String name) {
        return from(productsRepository.findProductsLikeName(name.toLowerCase()));
    }
    @Override
    public List<ProductDto> getAllProductsSortedById() {
        return from(productsRepository.findAllOrderByIdDesc());
    }

    @Override
    public List<ProductDto> getAllProductsSortedByName() {
        return from(productsRepository.findAllOrderByNameDesc());
    }

    @Override
    public List<ProductDto> getAllProductsSortedByCount() {
        return from(productsRepository.findAllOrderByCountDesc());
    }
    @Override
    public List<ProductDto> getAllProductsSortedBy(String sortBy) {
        List<ProductDto> products;
        switch (sortBy) {
            case ("id") -> products = getAllProductsSortedById();
            case ("name") -> products = getAllProductsSortedByName();
            case ("count") -> products = getAllProductsSortedByCount();
            default -> products = listOfProducts();
        }
        return products;
    }

}
