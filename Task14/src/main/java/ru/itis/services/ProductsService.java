package ru.itis.services;

import ru.itis.dto.ProductDto;

import java.util.List;

public interface ProductsService {
    List<ProductDto> signUp(ProductDto productData);
    List<ProductDto> listOfProducts();

    List<ProductDto> getProductLikeName(String productName);

    List<ProductDto> getAllProductsSortedById();

    List<ProductDto> getAllProductsSortedByName();

    List<ProductDto> getAllProductsSortedByCount();

    List<ProductDto> getAllProductsSortedBy(String sortBy);

}
