package ru.itis.dto.converters.http;

import jakarta.servlet.http.HttpServletRequest;
import ru.itis.dto.ProductDto;


public class HttpConverter {
    public static ProductDto from(HttpServletRequest request) {
    return ProductDto.builder()
            .name(request.getParameter("name"))
            .count(Integer.parseInt(request.getParameter("count")))
            .category(request.getParameter("category"))
            .build();
    }
}

