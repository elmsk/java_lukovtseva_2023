package ru.itis.controllers;

import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletResponse;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.itis.dto.ProductDto;
import ru.itis.services.ProductsService;

import java.io.IOException;
import java.util.List;

@RequiredArgsConstructor
@Controller
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@RequestMapping("/products")
public class ProductsController {

    ProductsService productsService;

    @RequestMapping
    public String getProductsPage(Model model, @CookieValue("sortBy") String sortBy) {
        model.addAttribute("products", productsService.getAllProductsSortedBy(sortBy));
        return "products";
    }

    @RequestMapping(value = "/profile")
    public String getProductsProfilePage() {
        return "profile";
    }

    @RequestMapping(value = "/liveSearch")
    @ResponseBody
    public List<ProductDto> liveSearchProducts(@RequestParam("query") String query) {
        return productsService.getProductLikeName(query);
    }

    @RequestMapping(method = RequestMethod.POST)
    @ResponseBody
    public List<ProductDto> addProduct(@RequestBody ProductDto productData) {
        return productsService.signUp(productData);
    }

    @RequestMapping(value = "/search")
    public String getFoundProductsPage(Model model, @RequestParam("productName") String productName) {
        model.addAttribute("products", productsService.getProductLikeName(productName));
        return "search_products";
    }

    @RequestMapping(value = "/sortChange", method = RequestMethod.POST)
    public void changeSorting(HttpServletResponse response, @RequestParam("sortBySelect") String sortBy) throws IOException {
        response.addCookie(new Cookie("sortBy", sortBy));
        response.sendRedirect("/products/profile");
    }
}
