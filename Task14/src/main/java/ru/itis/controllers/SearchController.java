package ru.itis.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class SearchController {

    @RequestMapping("/liveSearch/products")
    public String getLiveSearchPage() {
        return "live_search_products";
    }

    @RequestMapping("/search/products")
    public String getSearchPage() {
        return "search_products";
    }
}
