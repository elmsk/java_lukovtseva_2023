package ru.itis.models;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
@ToString
public class Product {
    private Long id;
    private String name;
    private Integer count;
    private String category;

}
