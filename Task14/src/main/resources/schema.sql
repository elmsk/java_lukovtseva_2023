drop table if exists product;

create table products(
    id bigserial primary key,
    name varchar(20),
    category varchar(30),
    count int check ( count >= 0 )
);

