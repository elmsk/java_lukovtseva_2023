package ru.itis.dto.converters.http;

import ru.itis.dto.ProductDto;
import ru.itis.dto.RegisterDto;

import javax.servlet.http.HttpServletRequest;

public class HttpConverter {public static ProductDto from(HttpServletRequest request) {
    return ProductDto.builder()
            .name(request.getParameter("name"))
            .count(Integer.parseInt(request.getParameter("count")))
            .category(request.getParameter("category"))
            .build();
}
    public static RegisterDto signUpDtoFrom(HttpServletRequest request) {
        return RegisterDto.builder()
                .firstName(request.getParameter("firstName"))
                .lastName(request.getParameter("lastName"))
                .email(request.getParameter("email"))
                .password(request.getParameter("password"))
                .build();
    }



}
