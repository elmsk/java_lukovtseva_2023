package ru.itis.constants;

public class Paths {

    public static final String ADD_PRODUCT_PATH = "/add";

    public static final String PRODUCTS_PATH = "/list";

    public static final String SEARCH_PRODUCTS_PATH = "/search";

    public static final String LIVE_SEARCH_PATH = "/liveSearch";

    public static final String SORT_PATH = PRODUCTS_PATH + "/sort";

    public static final String REGISTER_PATH = "/register";

    public static final String REGISTER_PAGE = "/register.html";

    public static final String LOG_IN_PATH = "/logIn";

    public static final String LOG_IN_PAGE = "/logIn.html";

    public static final String PROFILE_PATH = "/profile";

    public static final String PROFILE_PAGE = "/profile.html";

    public static final String WELCOME_PAGE = "/hello.html";

    public static final String LOGOUT = "/logout";
}
