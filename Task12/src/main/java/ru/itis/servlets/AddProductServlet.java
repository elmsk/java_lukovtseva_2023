package ru.itis.servlets;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.itis.config.ApplicationConfig;
import ru.itis.dto.ProductDto;
import ru.itis.dto.converters.http.HttpConverter;
import ru.itis.services.ProductsService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import static ru.itis.constants.Paths.ADD_PRODUCT_PATH;
import static ru.itis.constants.Paths.PRODUCTS_PATH;

@WebServlet(urlPatterns = {ADD_PRODUCT_PATH, PRODUCTS_PATH}, loadOnStartup = 1)
public class AddProductServlet extends HttpServlet {
    private ProductsService productsService;
    private HikariDataSource hikariDataSource;
    private ObjectMapper objectMapper;

    @Override
    public void init() throws ServletException {
        ApplicationContext context = new AnnotationConfigApplicationContext(ApplicationConfig.class);
        this.hikariDataSource = context.getBean(HikariDataSource.class);
        this.productsService = context.getBean(ProductsService.class);
        this.objectMapper = context.getBean(ObjectMapper.class);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (req.getRequestURI().equals(req.getContextPath() + PRODUCTS_PATH)) {
            List<ProductDto> products = getProductsList(req);

            resp.setContentType("text/html");
            PrintWriter writer = resp.getWriter();

            String html = getHtmlForProducts(products);

            writer.println(html);
        }
        else if (req.getRequestURI().equals(req.getContextPath() + ADD_PRODUCT_PATH)){
            req.getRequestDispatcher("webapp/add.html");
        }
        else {
            resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (req.getRequestURI().equals(req.getContextPath() + "/add")) {
            ProductDto productDto = HttpConverter.from(req);
            productsService.signUp(productDto);
            resp.sendRedirect(req.getContextPath() + "/list");
        }
        else if (req.getRequestURI().equals(PRODUCTS_PATH)) {
            String body = req.getReader().readLine();
            ProductDto productData = objectMapper.readValue(body, ProductDto.class);
            productsService.signUp(productData);
            List<ProductDto> products = productsService.listOfProducts();
            String jsonResponse = objectMapper.writeValueAsString(products);
            resp.setContentType("application/json");
            resp.getWriter().write(jsonResponse);
        }
        else {
                resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            }
    }
    private Cookie getCookieByName(String name, HttpServletRequest req) {
        Cookie[] cookies = req.getCookies();
        Cookie cookie;
        if (cookies != null) {
            for (Cookie c : cookies) {
                if (name.equals(c.getName())) {
                    cookie = c;
                    return cookie;
                }
            }
        }
        return null;
    }

    private List<ProductDto> getProductsList(HttpServletRequest req) {
        List<ProductDto> products;
        Cookie sortByCookie = getCookieByName("sort", req);
        String sortBy = "";
        if (sortByCookie != null) {
            sortBy = sortByCookie.getValue().trim();
        }
        switch (sortBy)  {
            case ("id") -> products = productsService.getAllProductsSortedById();
            case ("name") -> products = productsService.getAllProductsSortedByName();
            case ("count") -> products = productsService.getAllProductsSortedByCount();
            default -> products = productsService.listOfProducts();
        }
        return products;
    }


    private static String getHtmlForProducts(List<ProductDto> products) {
        StringBuilder html = new StringBuilder();

        html.append("<!DOCTYPE html>\n" +
                "<html>\n" +
                "<head>\n" +
                "\t<meta charset=\"utf-8\">\n" +
                "\t<title>Products</title>\n" +
                "</head>\n" +
                "<body>\n" +
                "<table>\n" +
                "\t<tr>\n" +
                "\t\t<th>ID</th>\n" +
                "\t\t<th>name</th>\n" +
                "\t\t<th>category</th>\n" +
                "\t\t<th>count</th>\n" +
                "\t</tr>");

        for (ProductDto productData : products) {
            html.append("<tr>\n");
            html.append("<td>").append(productData.getId()).append("</td>\n");
            html.append("<td>").append(productData.getName()).append("</td>\n");
            html.append("<td>").append(productData.getCategory()).append("</td>\n");
            html.append("<td>").append(productData.getCount()).append("</td>\n");
            html.append("</tr>\n");
        }

        html.append("</table>\n" +
                "</body>\n" +
                "</html>");
        return html.toString();
    }

    @Override
    public void destroy() {
        hikariDataSource.close();
    }
}

