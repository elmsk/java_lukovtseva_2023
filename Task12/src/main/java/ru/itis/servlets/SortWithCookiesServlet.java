package ru.itis.servlets;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static ru.itis.constants.Paths.PRODUCTS_PATH;
import static ru.itis.constants.Paths.SORT_PATH;

@WebServlet(urlPatterns = {SORT_PATH})
public class SortWithCookiesServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String sort = req.getParameter("sort");
        Cookie sortingCookie = new Cookie("sort", sort);
        sortingCookie.setPath("/");
        resp.addCookie(sortingCookie);
        resp.sendRedirect(PRODUCTS_PATH);

    }
}
