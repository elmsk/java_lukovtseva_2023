package ru.itis.servlets;

import org.springframework.context.ApplicationContext;
import ru.itis.dto.RegisterDto;
import ru.itis.dto.converters.http.HttpConverter;
import ru.itis.services.UsersService;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;

import static ru.itis.constants.Paths.PROFILE_PAGE;
import static ru.itis.constants.Paths.REGISTER_PATH;

public class RegistrationServlet extends HttpServlet {
    private UsersService usersService;

    @Override
    public void init(ServletConfig config) {
        ApplicationContext context = (ApplicationContext) config.getServletContext().getAttribute("springContext");
        this.usersService = context.getBean(UsersService.class);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        if (request.getRequestURI().equals(REGISTER_PATH)) {
            RegisterDto data = HttpConverter.signUpDtoFrom(request);
            usersService.signUp(data);
            response.sendRedirect(PROFILE_PAGE);
        }
        else {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
        }
    }

}
