package ru.itis.repositories;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;
import ru.itis.models.Product;

import javax.sql.DataSource;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class ProductsRepositoryJdbcTemplateImpl implements ProductsRepository{

    //language=SQL
    private static final String SQL_DELETE_PRODUCT = "delete from products where id = :id";

    //language=SQL
    private static final String SQL_SELECT_ALL_PRODUCTS = "select * from products;";
    //language=SQL
    private static final String SQL_SELECT_BY_CATEGORY = "select * from products where category = :category";
    //language=SQL
    private static final String SQL_SELECT_LIKE_NAME = "select * from products where name ilike '%' || :name || '%'";
    //language=SQL
    private static final String SQL_SELECT_PRODUCTS_ORDER_BY_ID_DESC = "select * from products order by id desc";
    //language=SQ
    private static final String SQL_SELECT_PRODUCTS_ORDER_BY_NAME_DESC = "select * from product order by product_name desc";
    //language=SQL
    private static final String SQL_SELECT_PRODUCTS_ORDER_BY_COUNT_DESC = "select * from products order by count desc";


    private final NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    public ProductsRepositoryJdbcTemplateImpl(DataSource dataSource) {
        this.namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }
    private static final RowMapper<Product> productMapper = (row, rowNumber) -> Product.builder()
            .id(row.getLong("id"))
            .name(row.getString("name"))
            .count(row.getObject("count", Integer.class))
            .category(row.getString("category"))
            .build();

    @Override
    public void save(Product product) {
        Map<String, Object> paramsAsMap = new HashMap<>();

        paramsAsMap.put("name", product.getName());
        paramsAsMap.put("category", product.getCategory());
        paramsAsMap.put("count", product.getCount());
        SimpleJdbcInsert insert = new SimpleJdbcInsert(namedParameterJdbcTemplate.getJdbcTemplate());

        Long id = insert.withTableName("products")
                .usingGeneratedKeyColumns("id")
                .executeAndReturnKey(new MapSqlParameterSource(paramsAsMap)).longValue();
        product.setId(id);
    }

    @Override
    public List<Product> findAll() {
        return namedParameterJdbcTemplate.query(SQL_SELECT_ALL_PRODUCTS, productMapper);
    }

    @Override
    public List<Product> findProductsByCategory(String category) {

        return namedParameterJdbcTemplate.query(SQL_SELECT_BY_CATEGORY, Collections.singletonMap("category", category), productMapper);
    }
    @Override
    public List<Product> findProductsLikeName(String name) {

        return namedParameterJdbcTemplate.query(SQL_SELECT_LIKE_NAME, Collections.singletonMap("name", name), productMapper);
    }
    @Override
    public List<Product> findAllOrderByIdDesc() {
        return namedParameterJdbcTemplate.query(SQL_SELECT_PRODUCTS_ORDER_BY_ID_DESC, productMapper);
    }

    @Override
    public List<Product> findAllOrderByNameDesc() {
        return namedParameterJdbcTemplate.query(SQL_SELECT_PRODUCTS_ORDER_BY_NAME_DESC, productMapper);
    }

    @Override
    public List<Product> findAllOrderByCountDesc() {
        return namedParameterJdbcTemplate.query(SQL_SELECT_PRODUCTS_ORDER_BY_COUNT_DESC, productMapper);
    }

}

