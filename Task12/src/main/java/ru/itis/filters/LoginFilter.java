package ru.itis.filters;

import org.springframework.context.ApplicationContext;
import ru.itis.security.AuthenticationManager;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

import static ru.itis.constants.Paths.LOG_IN_PATH;
import static ru.itis.constants.Paths.PROFILE_PATH;

@WebFilter(LOG_IN_PATH)
public class LoginFilter implements Filter {
    private AuthenticationManager authenticationManager;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        ApplicationContext context = (ApplicationContext) filterConfig.getServletContext().getAttribute("springContext");
        this.authenticationManager = context.getBean(AuthenticationManager.class);
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest)servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;

        if (request.getMethod().equals("POST")) {
            String email = request.getParameter("email");
            String password = request.getParameter("password");
            if (authenticationManager.authenticate(email, password)) {
                HttpSession session = request.getSession(true);
                session.setAttribute("authenticated", true);

                System.out.println(AuthFilter.fromURI + "------------");
                if (AuthFilter.fromURI != null) {
                    response.sendRedirect(AuthFilter.fromURI);
                } else {
                    response.sendRedirect(PROFILE_PATH);
                }
            } else {
                response.sendRedirect( LOG_IN_PATH + "?error");
            }
        } else {
            filterChain.doFilter(servletRequest, servletResponse);
        }
    }
}


