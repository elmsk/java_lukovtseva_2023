package ru.itis.filters;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

import static ru.itis.constants.Paths.*;

@WebFilter("/*")
public class AuthFilter implements Filter {
    public static String fromURI;

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;

        if (request.getRequestURI().equals(LOG_IN_PATH) ||
                request.getRequestURI().equals(REGISTER_PAGE) ||
                request.getRequestURI().equals(WELCOME_PAGE) ||
                request.getRequestURI().equals(REGISTER_PATH)) {
            filterChain.doFilter(request, response);
            return;
        }

        fromURI = request.getRequestURI();
        System.out.println(fromURI);

        if (isAuthenticated(request)) {
            filterChain.doFilter(request, response);
            return;
        }

        response.sendRedirect(LOG_IN_PATH);
    }

    private static boolean isAuthenticated(HttpServletRequest request) {
        HttpSession session = request.getSession(false);
        if (session != null) {
            Boolean authenticated = (Boolean) session.getAttribute("authenticated");
            return (authenticated != null && authenticated);
        }
        return false;
    }
}
