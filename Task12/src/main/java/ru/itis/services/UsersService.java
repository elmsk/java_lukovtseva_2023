package ru.itis.services;

import ru.itis.dto.RegisterDto;
import ru.itis.dto.UserDto;

import java.util.List;

public interface UsersService {
    void signUp(RegisterDto signUpData);
    List<UserDto> getAllUsers();
}
