package ru.itis.services.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.itis.dto.RegisterDto;
import ru.itis.dto.UserDto;
import ru.itis.models.User;
import ru.itis.repositories.UsersRepository;
import ru.itis.services.UsersService;

import java.util.List;

import static ru.itis.dto.UserDto.from;

@RequiredArgsConstructor
@Service
public class UsersServiceImpl implements UsersService {

    private final UsersRepository usersRepository;

    @Override
    public void signUp(RegisterDto signUpData) {
        User user = User.builder()
                .firstName(signUpData.getFirstName())
                .lastName(signUpData.getLastName())
                .email(signUpData.getEmail())
                .password(signUpData.getPassword())
                .build();

        usersRepository.save(user);
    }

    @Override
    public List<UserDto> getAllUsers() {
        return from(usersRepository.findAll());
    }
}

