package ru.itis.services.impl;

import lombok.RequiredArgsConstructor;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import ru.itis.dto.ProductDto;
import ru.itis.models.Product;
import ru.itis.repositories.ProductsRepository;
import ru.itis.services.ProductsService;
import java.util.List;

import static ru.itis.dto.ProductDto.from;
@RequiredArgsConstructor
@Service
public class ProductsServiceImpl implements ProductsService {

    private final ProductsRepository productsRepository;

    @Override
    public boolean signUp(ProductDto productData) {
        try {
            Product product = Product.builder()
                    .name(productData.getName())
                    .category(productData.getCategory())
                    .count(productData.getCount())
                    .build();
            productsRepository.save(product);
            return true;
        } catch (DataIntegrityViolationException e) {
            return false;
        }
    }
        @Override
    public List<ProductDto> listOfProducts() {
        return from(productsRepository.findAll());
    }
    @Override
    public List<Product> getProductsCategory(String category){
        return productsRepository.findProductsByCategory(category);
    }
    @Override
    public List<Product> getProductLikeName(String name) {
        return productsRepository.findProductsLikeName(name.toLowerCase());
    }
    @Override
    public List<ProductDto> getAllProductsSortedById() {
        return from(productsRepository.findAllOrderByIdDesc());
    }

    @Override
    public List<ProductDto> getAllProductsSortedByName() {
        return from(productsRepository.findAllOrderByNameDesc());
    }

    @Override
    public List<ProductDto> getAllProductsSortedByCount() {
        return from(productsRepository.findAllOrderByCountDesc());
    }

}
