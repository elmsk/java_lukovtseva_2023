package ru.itis.services;

import ru.itis.dto.ProductDto;
import ru.itis.models.Product;

import java.util.List;

public interface ProductsService {
    boolean signUp(ProductDto productData);
    List<ProductDto> listOfProducts();
    List<Product> getProductsCategory(String category);

    List<Product> getProductLikeName(String productName);

    List<ProductDto> getAllProductsSortedById();

    List<ProductDto> getAllProductsSortedByName();

    List<ProductDto> getAllProductsSortedByCount();

}
