create table products(
    id bigserial primary key,
    name varchar(20),
    category varchar(30),
    count int check ( count >= 0 )
);

create table users(
    id bigserial primary key,
    first_name varchar(20),
    last_name varchar(20),
    age int,
    email varchar(30),
    password varchar(30)
);