function searchProducts(query) {
    return fetch('/app/liveSearch?query=' + query)
        .then((response) => {
            return response.json()
        }).then((products) => {
            fillTable(products)
        })
}

function fillTable(products) {
    let table = document.getElementById("productsTable");

    table.innerHTML = "<tr>\n" +
        "      <th>id</th>\n" +
        "      <th>Name</th>\n" +
        "      <th>Count</th>\n" +
        "      <th>Category</th>\n" +
        "    </tr>";

    for (let i = 0; i < products.length; i++) {
        let row = table.insertRow(-1);
        let idCell = row.insertCell(0);
        let nameCell = row.insertCell(1);
        let countCell = row.insertCell(2);
        let categoryCell = row.insertCell(3);

        idCell.innerHTML = products[i].id;
        nameCell.innerHTML = products[i].name;
        countCell.innerHTML = products[i].count;
        categoryCell.innerHTML = products[i].category;
    }
}
function addProduct(name, count, category) {
    let body = {
        "name": name,
        "category": category,
        "count": count
    };

    fetch('/app/products', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(body)
    }).then((response) => response.json())
        .then((products) => fillTable(products))
        .catch((error) => {
            alert(error)
        })
}

