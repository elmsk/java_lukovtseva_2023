<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>User profile</title>
</head>
<body>
<h2>Your profile</h2>
<c:if test="${not empty user.imageId}">
    <img src="${pageContext.request.contextPath}/images?id=${user.imageId}"
         style="width: 100px"
         alt="Profile image"/>
</c:if>
<p>${user.firstName}</p>
<p>${user.lastName}</p>
<p>${user.age}</p>
<p><a href="${pageContext.request.contextPath}/upload"><button>Set profile image</button></a></p>
</body>
</html>
