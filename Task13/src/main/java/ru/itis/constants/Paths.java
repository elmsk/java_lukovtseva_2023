package ru.itis.constants;

public class Paths {

    public static final String LOG_IN_PATH = "/login";
    public static final String LOG_IN_PAGE = "/login.html";
    public static final String FILES_UPLOAD_PATH = "/upload";
    public static final String USER_PROFILE_PATH = "/profile";
    public static final String PROFILE_PAGE = "/startProfile.html";
    public static final String FILES_PATH = "/files";
    public static final String PROFILE_PATH = "/startProfile";

}
