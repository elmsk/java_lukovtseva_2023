package ru.itis.services;

import ru.itis.dto.FileDto;

public interface FileService {
    String upload(FileDto file);

    FileDto getFile(String fileName);

    FileDto getFile(Long id);

}
