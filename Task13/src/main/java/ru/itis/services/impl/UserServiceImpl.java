package ru.itis.services.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.itis.models.User;
import ru.itis.repositories.UserRepository;
import ru.itis.services.UserService;

@RequiredArgsConstructor
@Service
public class UserServiceImpl implements UserService {

    private final UserRepository usersRepository;

    @Override
    public User getUserByEmail(String email) {
        return usersRepository.findByEmail(email).orElseThrow();
    }
    @Override
    public void update(User user) {
        usersRepository.update(user);
    }

}
