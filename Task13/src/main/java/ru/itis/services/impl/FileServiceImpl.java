package ru.itis.services.impl;

import org.springframework.beans.factory.annotation.Value;
import ru.itis.dto.FileDto;
import ru.itis.models.FileInfo;
import ru.itis.repositories.FileRepository;
import ru.itis.services.FileService;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.UUID;

import static ru.itis.dto.FileDto.from;

public class FileServiceImpl implements FileService {
    @Value("${storage.path}")
    private String storagePath;

    private final FileRepository filesRepository;

    public FileServiceImpl(FileRepository filesRepository) {
        this.filesRepository = filesRepository;
    }

    @Override
    public String upload(FileDto file) {
        String originalFileName = file.getFileName();
        String extension = originalFileName.substring(originalFileName.lastIndexOf("."));
        String storageFileName = UUID.randomUUID() + extension;

        FileInfo fileInfo = FileInfo.builder()
                .description(file.getDescription())
                .size(file.getSize())
                .mimeType(file.getMimeType())
                .originalFileName(originalFileName)
                .storageFileName(storageFileName)
                .build();

        try {
            Files.copy(file.getFileStream(), Paths.get(storagePath + storageFileName));
        }
        catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

        filesRepository.save(fileInfo);
        return storageFileName;
    }

    @Override
    public FileDto getFile(String fileName) {
        FileInfo file = filesRepository.findByStorageFileName(fileName).orElseThrow();
        FileDto fileDto = from(file);
        fileDto.setPath(Paths.get(storagePath + "\\" + fileName));
        return fileDto;
    }

    @Override
    public FileDto getFile(Long id) {
        FileInfo file = filesRepository.findById(id).orElseThrow();
        return getFile(file.getStorageFileName());
    }

}
