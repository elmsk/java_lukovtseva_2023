package ru.itis.services;

import ru.itis.models.User;

public interface UserService {
    User getUserByEmail(String email);

    void update(User user);

}
