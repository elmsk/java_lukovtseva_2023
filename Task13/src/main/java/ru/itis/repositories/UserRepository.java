package ru.itis.repositories;

import ru.itis.models.User;

import java.util.Optional;

public interface UserRepository {
    Optional<User> findByEmail(String email);
    void update(User user);
}
