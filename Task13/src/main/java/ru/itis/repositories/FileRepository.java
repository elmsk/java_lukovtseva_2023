package ru.itis.repositories;

import ru.itis.models.FileInfo;

import java.util.List;
import java.util.Optional;

public interface FileRepository {
    void save(FileInfo fileInfo);

    Optional<FileInfo> findByStorageFileName(String fileName);

    Optional<FileInfo> findById(Long id);

    List<String> findAllStorageNamesByType(String ... types);

}
