package ru.itis.repositories.impl;

import org.springframework.stereotype.Repository;
import ru.itis.repositories.UserRepository;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import ru.itis.models.User;

import javax.sql.DataSource;
import java.util.*;


@Repository
public class UserRepositoryImpl implements UserRepository {
    //language=SQL
    private static final String SQL_SELECT_BY_EMAIL = "select * from users where email = :email";
    //language=SQL
    private static final String SQL_UPDATE_USER = "update users set first_name = :firstName, last_name = :lastName," +
            " age = :age, img_id = :imageId where id = :id";


    private static final RowMapper<User> userMapper = (row, rowNumber) -> User.builder()
            .id(row.getLong("id"))
            .firstName(row.getString("first_name"))
            .lastName(row.getString("last_name"))
            .age(row.getObject("age", Integer.class))
            .email(row.getString("email"))
            .password(row.getString("password"))
            .imageId(row.getLong("image_id"))
            .build();


    private final NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    public UserRepositoryImpl(DataSource dataSource) {
        this.namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    @Override
    public Optional<User> findByEmail(String email) {
        try {
            return Optional.ofNullable(namedParameterJdbcTemplate.queryForObject(SQL_SELECT_BY_EMAIL,
                    Collections.singletonMap("email", email),
                    userMapper));
        } catch (EmptyResultDataAccessException e) {
            return Optional.empty();
        }
    }
    @Override
    public void update(User user) {
        Map<String, Object> params = new HashMap<>();

        params.put("firstName", user.getFirstName());
        params.put("lastName", user.getLastName());
        params.put("age", user.getAge());
        params.put("imageId", user.getImageId());
        params.put("id", user.getId());

        namedParameterJdbcTemplate.update(SQL_UPDATE_USER, params);
    }
}
