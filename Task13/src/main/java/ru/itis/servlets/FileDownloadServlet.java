package ru.itis.servlets;

import org.springframework.context.ApplicationContext;
import ru.itis.dto.FileDto;
import ru.itis.services.FileService;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

import static ru.itis.constants.Paths.*;

@MultipartConfig
@WebServlet(FILES_PATH)
public class FileDownloadServlet extends HttpServlet {

    private FileService fileService;
    private List<String> imageTypes;

    @Override
    public void init(ServletConfig config) throws ServletException {
        ApplicationContext context = (ApplicationContext) config.getServletContext().getAttribute("springContext");
        this.fileService = context.getBean(FileService.class);
        imageTypes = new ArrayList<>(List.of("image/jpeg", "image/png", "image/jpg"));
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        FileDto file;
        if (request.getRequestURI().equals(FILES_PATH)) {
            String storageFileName = request.getParameter("fileName");
            file = fileService.getFile(storageFileName);
        } else {
            Long id = Long.parseLong(request.getParameter("id"));
            file = fileService.getFile(id);
            if (!imageTypes.contains(file.getMimeType())) {
                response.sendRedirect(FILES_UPLOAD_PATH + "?error");
            }
        }
        response.setContentType(file.getMimeType());
        response.setContentLength(file.getSize().intValue());
        response.setHeader("Content-Disposition", "filename=\"" + file.getOriginalFileName() + "\"");
        Files.copy(file.getPath(), response.getOutputStream());
        response.flushBuffer();
    }
}
