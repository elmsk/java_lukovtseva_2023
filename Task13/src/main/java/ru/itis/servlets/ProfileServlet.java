package ru.itis.servlets;

import org.springframework.context.ApplicationContext;
import ru.itis.models.User;
import ru.itis.services.UserService;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;

import static ru.itis.constants.Paths.USER_PROFILE_PATH;

@WebServlet(USER_PROFILE_PATH)
public class ProfileServlet extends HttpServlet {
    private UserService usersService;

    @Override
    public void init(ServletConfig config) throws ServletException {
        ApplicationContext context = (ApplicationContext) config.getServletContext().getAttribute("springContext");
        this.usersService = context.getBean(UserService.class);
    }
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        User user = usersService.getUserByEmail((String) req.getSession().getAttribute("email"));
        req.setAttribute("user", user);
        req.getServletContext().getRequestDispatcher("/profile.jsp").forward(req, resp);
    }

}
