package ru.itis.servlets;

import org.springframework.context.ApplicationContext;
import ru.itis.dto.FileDto;
import ru.itis.models.User;
import ru.itis.repositories.FileRepository;
import ru.itis.services.FileService;
import ru.itis.services.UserService;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import java.io.IOException;

import static ru.itis.constants.Paths.FILES_UPLOAD_PATH;
import static ru.itis.constants.Paths.USER_PROFILE_PATH;

@WebServlet(FILES_UPLOAD_PATH)
@MultipartConfig
public class FilesUploadServlet extends HttpServlet {
    private FileService filesService;
    private UserService usersService;
    private FileRepository filesRepository;

    @Override
    public void init(ServletConfig config) throws ServletException {
        ApplicationContext context = (ApplicationContext) config.getServletContext().getAttribute("springContext");
        this.filesService = context.getBean(FileService.class);
        this.usersService = context.getBean(UserService.class);
        this.filesRepository = context.getBean(FileRepository.class);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getServletContext().getRequestDispatcher("/setProfileImage.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        Part filePart = request.getPart("file");

        FileDto uploadedFileInfo = FileDto.builder()
                .size(filePart.getSize())
                .mimeType(filePart.getContentType())
                .fileName(filePart.getSubmittedFileName())
                .fileStream(filePart.getInputStream())
                .build();

        String storageFileName = filesService.upload(uploadedFileInfo);

        User user = usersService.getUserByEmail((String) request.getSession().getAttribute("email"));
        user.setImageId(filesRepository.findByStorageFileName(storageFileName).orElseThrow().getId());
        usersService.update(user);

        response.sendRedirect(USER_PROFILE_PATH);
    }
}
