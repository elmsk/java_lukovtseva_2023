drop table if exists products;
create table products (
    id bigserial primary key,
    name varchar(40),
    price float,
    count int,
    color varchar(20) default null,
    type_of_product varchar(40)
);