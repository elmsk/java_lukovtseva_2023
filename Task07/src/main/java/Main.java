import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import modules.Product;
import repositories.ProductsRepository;
import repositories.ProductsRepositoryNamedJdbcTemplateImpl;

import java.io.IOException;
import java.util.Properties;

public class Main {
    public static void main(String[] args) {
        Properties properties = new Properties();
        try {
            properties.load(Main.class.getResourceAsStream("/db.properties"));
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

        HikariConfig hikariConfig = new HikariConfig();
        hikariConfig.setJdbcUrl(properties.getProperty("db.url"));
        hikariConfig.setUsername(properties.getProperty("db.username"));
        hikariConfig.setPassword(properties.getProperty("db.password"));
        hikariConfig.setMaximumPoolSize(10);

        HikariDataSource dataSource = new HikariDataSource(hikariConfig);

        ProductsRepository productsRepository = new ProductsRepositoryNamedJdbcTemplateImpl(dataSource);

        //System.out.println(productsRepository.findById(1L));

        //System.out.println(productsRepository.findAll());

        //System.out.println(productsRepository.findAllByColorOrderByIdDesc("-"));

        Product p = Product.builder()
                .name("Вода питьевая")
                .price(22.49)
                .count(560)
                .color("-")
                .typeOfProduct("Вода")
                .build();
        Product p1 = Product.builder()
                .id(2L)
                .name("Вода питьевая")
                .price(22.49)
                .count(550)
                .color("-")
                .typeOfProduct("Вода")
                .build();
        //productsRepository.save(p);
        //productsRepository.delete(1L);
        productsRepository.update(p1);

    }
}
