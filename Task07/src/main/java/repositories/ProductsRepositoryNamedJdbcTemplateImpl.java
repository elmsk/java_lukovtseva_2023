package repositories;

import modules.Product;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;

import javax.sql.DataSource;
import java.util.*;

public class ProductsRepositoryNamedJdbcTemplateImpl implements ProductsRepository{

    private final NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    public ProductsRepositoryNamedJdbcTemplateImpl(DataSource dataSource) {
        this.namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    //language = SQL
    private final String SQL_SELECT_BY_ID = "select * from products where id = :id";

    //language=SQL
    private static final String SQL_SELECT_ALL_PRODUCTS = "select * from products;";

    //language=SQL
    private static final String SQL_DELETE_PRODUCT = "delete from products where id = :id";

    //language=SQL
    private static final String SQL_UPDATE_PRODUCT_USING_ID = "update products " +
            "set name = :name, price = :price, " +
            "count = :count, color = :color, " +
            "type_of_product = :type_of_product " +
            "where id = :id";
    //language=SQL
    private static final String SQL_SELECT_ALL_BY_COLOR_ORDER_BY_ID_DESC = "select * from products " +
            "where color = :color order by id";

    private static final RowMapper<Product> productMapper = (row, rowNumber) -> Product.builder()
            .id(row.getLong("id"))
            .name(row.getString("name"))
            .price(row.getDouble("price"))
            .count(row.getObject("count", Integer.class))
            .color(row.getString("color"))
            .typeOfProduct(row.getString("type_of_product"))
            .build();

    @Override
    public Optional<Product> findById(Long id) {
        try {
            return Optional.ofNullable(namedParameterJdbcTemplate.queryForObject(SQL_SELECT_BY_ID,
                    Collections.singletonMap("id", id), productMapper));
        }
        catch (EmptyResultDataAccessException ex){
            return Optional.empty();
        }
    }

    @Override
    public void update(Product product) {
        Map<String, Object> paramsAsMap = new HashMap<>();

        paramsAsMap.put("id", product.getId());
        paramsAsMap.put("name", product.getName());
        paramsAsMap.put("price", product.getPrice());
        paramsAsMap.put("count", product.getCount());
        paramsAsMap.put("color", product.getColor());
        paramsAsMap.put("type_of_product", product.getTypeOfProduct());

        namedParameterJdbcTemplate.update(SQL_UPDATE_PRODUCT_USING_ID, paramsAsMap);
    }

    @Override
    public void delete(Long id) {
        namedParameterJdbcTemplate.update(SQL_DELETE_PRODUCT, Collections.singletonMap("id", id));
    }

    @Override
    public List<Product> findAll() {
        return namedParameterJdbcTemplate.query(SQL_SELECT_ALL_PRODUCTS, productMapper);
    }

    @Override
    public void save(Product product) {
        Map<String, Object> paramsAsMap = new HashMap<>();

        paramsAsMap.put("name", product.getName());
        paramsAsMap.put("price", product.getPrice());
        paramsAsMap.put("count", product.getCount());
        paramsAsMap.put("color", product.getColor());
        paramsAsMap.put("type_of_product", product.getTypeOfProduct());

        SimpleJdbcInsert insert = new SimpleJdbcInsert(namedParameterJdbcTemplate.getJdbcTemplate());

        Long id = insert.withTableName("products")
                .usingGeneratedKeyColumns("id")
                .executeAndReturnKey(new MapSqlParameterSource(paramsAsMap)).longValue();
        product.setId(id);
    }
    @Override
    public List<Product> findAllByColorOrderByIdDesc(String color) {
        return namedParameterJdbcTemplate.query(SQL_SELECT_ALL_BY_COLOR_ORDER_BY_ID_DESC, Collections.singletonMap("color", color), productMapper);
    }

}
