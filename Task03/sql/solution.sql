-- 1 task
select model, speed, hd
from PC
where PC.price < 500;

-- 2 task
select distinct maker
from Product
where Product.type = 'Printer';

-- 3 task
select model, ram, screen
from Laptop
where Laptop.price > 1000;

-- 4 task
select code, model, color, type, price
from Printer
where color = 'y';

-- 5 task
select model, speed, hd
from PC
where (PC.cd = '12x' OR PC.cd = '24x') AND PC.price < 600;

-- 6 task
select distinct Product.maker, Laptop.speed
from Laptop join Product Product on Product.model = Laptop.model
where Laptop.hd >= 10;

-- 7 task
select distinct Product.model, PC.price
from Product join PC PC on Product.model = PC.model
where maker = 'B'
union
select distinct Product.model, Laptop.price
from Product join Laptop Laptop on Laptop.model = Product.model
where maker = 'B'
union
select distinct Product.model, Printer.price
from Product join Printer Printer on Printer.model = Product.model
where maker = 'B';

-- 8 task
select maker
from Product
where type = 'PC'
EXCEPT
select maker
from product
where type = 'Laptop';

-- 9 task
select distinct maker
from PC join Product on PC.model = Product.model
where speed >= 450;

-- 10 task
select model, price
from Printer
where price = (select MAX(price) from Printer);

-- 11 task
select AVG(speed)
from PC;

-- 12 task
select AVG(speed)
from Laptop
where price > 1000;

-- 13 task
select AVG(speed)
from PC join Product on PC.model = Product.model
where Product.maker = 'A';

-- 14 task
select Ships.class, Ships.name, Classes.country
from Ships left join Classes on Ships.class = Classes.class
where Classes.numGuns >= 10;

-- 15 task
select hd
from PC
group by hd having count(model) >= 2;

-- 16 task
select distinct PC1.model, PC2.model, PC1.speed, PC1.ram
from PC PC1, PC PC2
where PC1.speed = PC2.speed AND PC1.ram = PC2.ram AND PC1.model > PC2.model;

-- 17 task
select distinct Product.type, Product.model, Laptop.speed
from Laptop join Product on Laptop.model = Product.model
where Laptop.speed < (select MIN(speed) from PC);

-- 18 task
select distinct maker, Printer.price
from Product join Printer on Product.model = Printer.model
where Printer.color = 'y' AND Printer.price = (select MIN(price) from printer where Printer.color = 'y');

-- 19 task
select maker, AVG(screen)
from Laptop left join Product on Product.model = Laptop.model
group by maker;

-- 20 task
select maker, COUNT(model)
from Product
where type = 'PC'
group by maker having COUNT(distinct model) >= 3;

-- 21 task
select maker, MAX(price)
from Product join PC on product.model = pc.model
group by maker;

-- 22 task
select speed, AVG(price)
from PC
where speed > 600
group by speed;

-- 23 task
select distinct maker
from Product P1 join PC P2 on P1.model = P2.model
where speed >= 750 AND maker in (select maker
                                 from Product P1 join Laptop L1 on P1.model = L1.model
                                 where speed >= 750);

-- 24 task
select model
from (
         select model, price from PC
         union
         select model, price from Laptop
         union
         select model, price from Printer) t1
where price = (
    select MAX(price) from (
                               select price from PC
                               union
                               select price from Laptop
                               union
                               select price from Printer
                           ) t2;
)

-- 25 task
select distinct maker
from product
where model in (
    select model from PC where ram = (
        select MIN(ram)
        from PC
    )
    AND speed = (
        select MAX(speed)
        from PC
        where ram = (select MIN(ram) from PC)
    )
)
AND
    maker in (
        select maker
        from Product
        where type ='Printer'
    );

-- 26 task
select AVG(price)
from (
    select code, price, PC.model, ram, hd
    from PC
    where model in (
        select model
        from Product
        where maker = 'A'
         )
    union
    select code, price, Laptop.model, ram, hd
    from Laptop
    where model in (
        select model
        from Product
        where maker = 'A'
        )
    ) t1;

-- 27 task
select maker, AVG(hd)
from PC join Product on Product.model = PC.model
where maker in (select distinct maker from Product
                where type = 'Printer')
group by maker;

-- 28 task
select count(maker)
from product
where maker in (
    select maker from Product
    group by maker
    having count(model) = 1
    )

-- 29 task
select t1.point, t1.date, inc, out
from Income_o t1 left join Outcome_o t2 on t1.point = t2.point
    AND t1.date = t2.date
union
select t2.point, t2.date, inc, out
from Income_o t1 right join Outcome_o t2 on t1.point = t2.point
    AND t1.date = t2.date;

-- 30 task
select point, date, SUM(sum_out), SUM(sum_inc)
from (
    select point, date, SUM(inc) as sum_inc, null as sum_out
    from Income
    group by point, date
    union
    select point, date, null as sum_inc, SUM(out) as sum_out
    from Outcome group by point, date
    ) as t
group by point, date order by point;

-- 31 task
select distinct class, country
from Classes
where bore >= 16;

-- 32 task
select country, cast(AVG((power(bore,3)/2)) as numeric(6,2)) as weight
from (
    select country, Classes.class, bore, name
    from Classes left join ships on Classes.class = Ships.class
    union all
    select distinct country, class, bore, ship
    from Classes t1 left join Outcomes t2 on t1.class = t2.ship
    where ship = class and ship not in (select name from ships)
    ) a
where name is not null
group by country;

-- 33 task
select Outcomes.ship
from Battles left join Outcomes on Outcomes.battle = Battles.name
where Battles.name = 'North Atlantic' AND Outcomes.result = 'sunk';

-- 34 task
select name
from Classes join Ships on Ships.class = Classes.class
where launched >= 1922 AND displacement > 35000 AND type='bb';

-- 35 task
select model, type
from Product
where upper(model) not like '%[^A-Z]%' OR model not like '%[^0-9]%';

-- 36 task
select name
from Ships
where class = name
union
select ship as name
from Classes join Outcomes on Classes.class = Outcomes.ship;

-- 37 task
select c.class
from Classes c left join (
    select class, name
    from Ships
    union
    select ship, ship
    from Outcomes
) as s on s.class = c.class
group by c.class
having count(s.name) = 1;


-- 38 task
select country
from Classes
group by country
having COUNT(distinct type) = 2;

-- 39 task
select distinct B.ship
from (
    select *
    from Outcomes left join battles on battle = name
    where result = 'damaged'
    ) as B
where exists (
    select ship
    from Outcomes left join battles on battle = name
    where ship = B.ship and B.date < date
    )

-- 40 task
select maker, MAX(type)
from Product
group by maker
having COUNT(distinct type) = 1 AND COUNT(model) > 1;



