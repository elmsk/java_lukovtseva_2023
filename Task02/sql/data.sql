insert into client (nickname, rating)
values ('FrutoНяня', 4.78),
       ('AlexDobrov21', 4.99),
       ('ElenaElena', 4.95);

insert into driver (first_name, last_name, rating)
values ('Александр', 'Александров', 4.9),
       ('Николай', 'Твой лалилалай', 4.7),
       ('Геральт', 'Из Ривии', 4.95);

insert into car (model, number)
values ('Lada Vesta 2021', 'в777ор77'),
       ('Lada Granta 2020', 'в007ор77'),
       ('Mazda X5 2021', 'в001ор11');

update client
set nickname = 'NotElenaAnymore'
where id = 3;

update driver
set car_id = 2
where id = 2;

update driver
set car_id = 1
where id = 1;


update driver
set car_id = 3
where id = 3;



insert into taxi_order(city, address_from, address_to, cost, client_id, driver_id)
values ('Москва', 'Генерала Рычагова, 99', 'Адмирала Макарова, 10', 400, 1, 1),
       ('Казань', 'Баки Урманче, 7', 'Кремлёвская, 35', 250, 3, 2),
       ('Москва', 'Ленинградское шоссе, 55', 'тц Авиапарк', 360, 2 ,3);


