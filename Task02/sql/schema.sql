DROP table if exists client CASCADE;
DROP table if exists driver CASCADE;
drop table if exists taxi_order;
DROP table if exists car CASCADE;

create table client (
    id bigserial primary key,
    nickname char(20),
    rating float check (rating > 0 and rating <= 5.00) not null default 5.00
);
create table driver (
    id bigserial primary key,
    first_name char(20),
    last_name char(20),
    car_id bigint,
    rating float check (rating > 0 and rating <= 5.00) not null default 5.00
);
create table car (
    id bigserial primary key,
    model char(20),
    number char(20)
);
create table taxi_order (
    id bigserial primary key,
    city char(20),
    address_from char(40),
    address_to char(40),
    cost int,
    client_id bigint,
    driver_id bigint
);
alter table driver add foreign key(car_id) references car(id);
alter table taxi_order add foreign key(driver_id) references driver(id);
alter table taxi_order add foreign key(client_id) references client(id);