package ru.kpfu.itis;

import ru.kpfu.itis.jdbc.SimpleDataSource;
import ru.kpfu.itis.models.Student;
import ru.kpfu.itis.repositories.StudentsRepository;
import ru.kpfu.itis.repositories.StudentsRepositoryJdbcImpl;

import javax.sql.DataSource;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class Main {


    public static void main(String[] args) {
        Properties properties = new Properties();
        try {
            properties.load(new FileInputStream("resources\\db.properties"));
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

        DataSource dataSource = new SimpleDataSource(
                properties.getProperty("db.url"),
                properties.getProperty("db.username"),
                properties.getProperty("db.password")
        );

        StudentsRepository studentsRepository = new StudentsRepositoryJdbcImpl(dataSource);

        System.out.println(studentsRepository.findAll());

        Student student = new Student("Катя", "Луковцева", 19);

        studentsRepository.save(student);

        System.out.println(studentsRepository.findById(Long.parseLong("6")));
    }
}
