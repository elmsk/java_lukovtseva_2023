package ru.itis.ui;

import ru.itis.services.ProductService;

import java.util.Scanner;

public class UI {

    private final Scanner scanner = new Scanner(System.in);

    private final ProductService productService;

    public UI(ProductService productService) {
        this.productService = productService;
    }
    public void start() {
        while (true) {
            printMainMenu();

            String command = scanner.nextLine();

            switch (command){
                case "1" -> {
                    String name = scanner.nextLine();
                    if (this.productService.signUp(name)) {
                        System.out.println("Товар добавлен");
                    }
                    else {
                        System.err.println("Неверно введены данные. Повторите попытку.");
                    }
                }
                case "2" -> System.out.println("Список товаров: " + "\n" + productService.listOfProducts().toString());
                case "3" -> {
                    long id = Long.parseLong(scanner.nextLine());
                    if (this.productService.deleteProduct(id)) {
                        System.out.println("Товар успешно удален");
                    }
                    else {
                        System.err.println("Неверно введен id товара. Повторите попытку.");
                    }
                }
                case "4" -> System.exit(0);
                default -> System.out.println("Команда не распознана");
                }
            }

        }

    private void printMainMenu(){
        System.out.println("Выберите действие:");
        System.out.println("1. Добавить товар (максимум 40 символов)");
        System.out.println("2. Получить список товаров");
        System.out.println("3. Удалить товар");
        System.out.println("4. Выход");
    }
}
