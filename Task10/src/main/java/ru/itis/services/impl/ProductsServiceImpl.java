package ru.itis.services.impl;

import org.postgresql.util.PSQLException;
import org.springframework.dao.DataIntegrityViolationException;
import ru.itis.models.Product;
import ru.itis.repositories.ProductsRepository;
import ru.itis.services.ProductService;

import java.util.List;

public class ProductsServiceImpl implements ProductService {

    private final ProductsRepository productsRepository;

    public ProductsServiceImpl(ProductsRepository productsRepository) {
        this.productsRepository = productsRepository;
    }

    @Override
    public boolean signUp(String name) {
        Product product = Product.builder()
                .name(name)
                .build();
        try {
            productsRepository.save(product);
        }
        catch (DataIntegrityViolationException ex){
            return false;
        }
        return true;
    }

    @Override
    public List<Product> listOfProducts() {
        return productsRepository.findAll();
    }

    @Override
    public boolean deleteProduct(Long id) {
        return productsRepository.delete(id) != 0;
    }
}
