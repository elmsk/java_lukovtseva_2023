package ru.itis.services;

import ru.itis.models.Product;

import java.util.List;

public interface ProductService {
    boolean signUp(String name);
    List<Product> listOfProducts();
    boolean deleteProduct(Long id);
}
