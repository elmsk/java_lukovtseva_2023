package ru.itis;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

@Mojo(name = "size-of-classes", defaultPhase = LifecyclePhase.COMPILE)

public class SizeOfClassesMojo extends AbstractMojo {

    @Parameter(defaultValue = "${project.build.outputDirectory}", required = true)
    private String outputFolderFileName;

    @Parameter(defaultValue = "${project.build.sourceDirectory}", required = true, readonly = true)
    private String sourceFolderFileName;

    @Parameter(name = "sizeOfClassesFileName", required = true)
    private String sizeOfClassesFileName;

    @Override
    public void execute() throws MojoExecutionException {
        File outputFolder = new File(outputFolderFileName);
        File sizeOfClasses = new File(outputFolder, sizeOfClassesFileName);

        try (BufferedWriter writer = new BufferedWriter(new FileWriter(sizeOfClasses))){
            getLog().info("Output file for size of classes is - "
                    + sizeOfClassesFileName);

            Files.walk(Paths.get(sourceFolderFileName))
                    .filter(Files::isRegularFile)
                    .forEach(file -> {
                        try {
                            writer.write(file.getFileName().toString()
                                    + "  "
                                    + humanReadableSizeOfFile(file.toFile().length()));
                        }
                        catch (IOException ex){
                            throw new IllegalArgumentException(ex);
                        }
                    });
        }
        catch (IOException ex){
            throw new MojoExecutionException(ex);
        }
    }
    private String humanReadableSizeOfFile(long size){
        int flag = 0;
        while (size >= 1024 && flag <= 2){
            size /= 1024;
            flag++;
        }
        String[] sizes = {" B", " KB", " MB"};
        return size + sizes[flag];
    }
}
