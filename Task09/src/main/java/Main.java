import generators.SqlGenerator;
import models.User;

public class Main {
    public static void main(String[] args) {
        User user = new User("Kate", "Kate", true);
        SqlGenerator sqlGenerator = new SqlGenerator();
        System.out.println(sqlGenerator.createTable(User.class));
        System.out.println(sqlGenerator.insert(user));
    }
}
