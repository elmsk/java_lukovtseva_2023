package annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface ColumnName {
    String columnName();
    boolean primary() default false;
    boolean identity() default false;
    int maxLength() default 256;
    boolean defaultBoolean() default false;
}
