package generators;

import annotations.ColumnName;
import annotations.TableName;
import java.lang.reflect.Field;

public class SqlGenerator {

    public String createTable(Class<?> entityClass){
        TableName tableName = entityClass.getDeclaredAnnotation(TableName.class);
        String SQL = "create table " + tableName.tableName() + " (";

        Field[] fields = entityClass.getDeclaredFields();

        for (Field field : fields) {
            String primary = "";
            String maxLength = "";
            String defaultBoolean = "";
            String identity = "";
            ColumnName columnName = field.getDeclaredAnnotation(ColumnName.class);
            String type = typeOfColumn(field.getType());
            if (columnName.primary()){
                primary = "primary key";
            }
            if (columnName.identity()){
                identity = "GENERATED ALWAYS AS IDENTITY ";
            }
            if (field.getType().equals(String.class)){
                maxLength = "" + columnName.maxLength();
                type += maxLength + ") ";
            }
            if (field.getType().equals(Boolean.TYPE)){
                defaultBoolean = "" + columnName.defaultBoolean();
            }
            if (defaultBoolean.equals("")){
                SQL += columnName.columnName() + " " + type + " " + identity + primary + ",";
            }
            else {
                SQL += columnName.columnName() + " " + type + " " + identity + primary + " default " + defaultBoolean + ",";
            }
        }
        return SQL.substring(0, SQL.length() - 1) + ");";
    }

    public String insert(Object entity){
        Class<?> aClass = entity.getClass();
        TableName tableName = aClass.getDeclaredAnnotation(TableName.class);
        String SQL = "insert into " + tableName.tableName() + " values ( ";
        Field[] fields = aClass.getDeclaredFields();

        for (Field field: fields){
            field.setAccessible(true);
            ColumnName columnName = field.getDeclaredAnnotation(ColumnName.class);
            if(columnName.primary() || columnName.identity()){
                continue;
            }
            try {
                Object obj = field.get(entity);
                if (obj.getClass().equals(String.class)){
                    SQL += "'" + obj + "'" + ",";
                }
                else{
                    SQL += "" + obj + ",";
                }
            }
            catch (IllegalAccessException e) {
                throw new RuntimeException(e);
            }
        }
        return SQL.substring(0, SQL.length() -1) + ");";
    }

    private String typeOfColumn(Class<?> aClass){
        if (aClass.equals(String.class)){
            return "varchar(";
        }
        if (aClass.equals(Integer.TYPE)){
            return "int";
        }
        if (aClass.equals(Double.TYPE) || aClass.equals(Float.TYPE)){
            return "float";
        }
        if (aClass.equals(Long.class)){
            return "bigint";
        }
        if (aClass.equals(Boolean.TYPE)){
            return "boolean";
        }
        return null;
    }
}
