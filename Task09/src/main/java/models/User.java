package models;

import annotations.ColumnName;
import annotations.TableName;

@TableName(tableName = "account")
public class User {
    @ColumnName(columnName = "id",  primary = true, identity = true)
    private Long id;

    @ColumnName(columnName = "firstName", maxLength = 25)
    private String firstName;

    @ColumnName(columnName = "lastName")
    private String lastName;

    @ColumnName(columnName = "is_worker", defaultBoolean = true)
    private boolean isWorker;

    public User(String firstName, String lastName, boolean isWorker) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.isWorker = isWorker;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", isWorker=" + isWorker +
                '}';
    }
}
