package ru.itis.constants;

public class Paths {
    public static final String APPLICATION_PREFIX = "/app";

    public static final String ADD_PRODUCT_PATH = "/add";

    public static final String PRODUCTS_PATH = "/list";

    public static final String SEARCH_PRODUCTS_PATH = "/search";

    public static final String LIVE_SEARCH_PAGE = "/liveSearch.html";

    public static final String LIVE_SEARCH_PATH = "/liveSearch";

}
