package ru.itis.services.impl;

import lombok.RequiredArgsConstructor;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import ru.itis.models.Product;
import ru.itis.repositories.ProductsRepository;
import ru.itis.services.ProductsService;
import java.util.List;

@RequiredArgsConstructor
@Service
public class ProductsServiceImpl implements ProductsService {

    private final ProductsRepository productsRepository;

    @Override
    public boolean signUp(String name, int count, String category) {
        Product product = Product.builder()
                .name(name)
                .category(category)
                .count(count)
                .build();
        productsRepository.save(product);

        try {
            productsRepository.save(product);
        }
        catch (DataIntegrityViolationException ex){
            return false;
        }
        return true;
    }

    @Override
    public List<Product> listOfProducts() {
        return productsRepository.findAll();
    }
    @Override
    public List<Product> getProductsCategory(String category){
        return productsRepository.findProductsByCategory(category);
    }
    @Override
    public List<Product> getProductLikeName(String productName) {
        return productsRepository.findProductsLikeName(productName.toLowerCase());
    }
}
