package ru.itis.services;

import ru.itis.models.Product;

import java.util.List;

public interface ProductsService {
    boolean signUp(String name, int count, String category);
    List<Product> listOfProducts();
    List<Product> getProductsCategory(String category);

    List<Product> getProductLikeName(String productName);
}
