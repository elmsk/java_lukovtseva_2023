package ru.itis.dto.converters.http;

import ru.itis.dto.ProductDto;

import javax.servlet.http.HttpServletRequest;

public class HttpConverter {
    public static ProductDto from(HttpServletRequest request) {
        return ProductDto.builder()
                .productName(request.getParameter("name"))
                .count(Integer.parseInt(request.getParameter("count")))
                .category(request.getParameter("category"))
                .build();
    }
}
