package ru.itis.repositories;

import ru.itis.models.Product;

import java.util.List;

public interface ProductsRepository {
    void save(Product product);
    List<Product> findAll();
    int delete(Long id);
    List<Product> findProductsByCategory(String category);
    List<Product> findProductsLikeName(String name);
}
