package ru.itis.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class Product {
    private Long id;
    private String name;
    private Integer count;
    private String category;

    public Product(String name, int count, String category){
        this.name = name;
        this.count = count;
        this.category = category;
    }
}
