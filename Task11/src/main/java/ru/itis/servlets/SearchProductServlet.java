package ru.itis.servlets;

import com.zaxxer.hikari.HikariDataSource;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.itis.config.ApplicationConfig;
import ru.itis.models.Product;
import ru.itis.services.ProductsService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;


import java.util.List;

import static ru.itis.constants.Paths.SEARCH_PRODUCTS_PATH;


@WebServlet(urlPatterns = {SEARCH_PRODUCTS_PATH}, loadOnStartup = 1)
public class SearchProductServlet extends HttpServlet {
    private ProductsService productsService;
    private HikariDataSource hikariDataSource;

    @Override
    public void init() throws ServletException {
        ApplicationContext context = new AnnotationConfigApplicationContext(ApplicationConfig.class);
        this.hikariDataSource = context.getBean(HikariDataSource.class);
        this.productsService = context.getBean(ProductsService.class);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (req.getRequestURI().equals(req.getContextPath() + "/search")
                && req.getParameter("category") != null) {
            List<Product> products;
            String category = req.getParameter("category").trim();

            products = productsService.getProductsCategory(category);

            resp.setContentType("text/html");
            PrintWriter writer = resp.getWriter();

            String html = getHtmlForProducts(products);

            writer.println(html);
        }
        else {
            resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
        }
    }

    private static String getHtmlForProducts(List<Product> products) {
        StringBuilder html = new StringBuilder();

        html.append("<!DOCTYPE html>\n" +
                "<html>\n" +
                "<head>\n" +
                "\t<meta charset=\"utf-8\">\n" +
                "\t<title>Products</title>\n" +
                "</head>\n" +
                "<body>\n" +
                "<table>\n" +
                "\t<tr>\n" +
                "\t\t<th>ID</th>\n" +
                "\t\t<th>Name</th>\n" +
                "\t\t<th>Category</th>\n" +
                "\t\t<th>Count</th>\n" +
                "\t</tr>");

        for (Product product : products) {
            html.append("<tr>\n");
            html.append("<td>").append(product.getId()).append("</td>\n");
            html.append("<td>").append(product.getName()).append("</td>\n");
            html.append("<td>").append(product.getCategory()).append("</td>\n");
            html.append("<td>").append(product.getCount()).append("</td>\n");
            html.append("</tr>\n");
        }

        html.append("</table>\n" +
                "</body>\n" +
                "</html>");
        return html.toString();
    }

    @Override
    public void destroy() {
        hikariDataSource.close();
    }
}
