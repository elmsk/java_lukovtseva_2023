drop table if exists products;

create table products (
    id bigserial primary key,
    name varchar(30),
    category varchar(30),
    count int
);
