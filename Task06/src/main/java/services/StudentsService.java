package services;

import dto.StudentSignUp;

/**
 * 11.07.2022
 * 03. Database
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public interface StudentsService {
    void signUp(StudentSignUp form);
}
