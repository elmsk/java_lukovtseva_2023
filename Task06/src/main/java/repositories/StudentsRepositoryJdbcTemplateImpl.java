package repositories;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlParameterValue;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import models.Student;
import org.springframework.jdbc.object.SqlUpdate;

import javax.sql.DataSource;
import java.sql.*;
import java.util.*;

/**
 * 08.07.2022
 * 03. Database
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class StudentsRepositoryJdbcTemplateImpl implements StudentsRepository {

    //language=SQL
    private static final String SQL_SELECT_ALL_STUDENTS = "select * from student;";

    //language=SQL
    private static final String SQL_SELECT_BY_ID = "select * from student where id = ?";

    //language=SQL
    private static final String SQL_UPDATE_STUDENT_USING_ID = "update student set first_name = ?, last_name = ?, age = ?, email = ?, password = ? " +
            "where id = ?";

    //language=SQL
    private static final String SQL_DELETE_STUDENT = "delete from student_course where student_id = ?;" +
            "delete from student where id = ?";

    //language=SQL
    private static final String SQL_FIND_STUDENTS_WITH_AGE_GREATER_THAN_MINAGE_ORDER_BY_ID_DESC = "select * from student " +
            "where age > ? order by id desc";

    private static final RowMapper<Student> studentMapper = (row, rowNumber) -> Student.builder()
            .id(row.getLong("id"))
            .firstName(row.getString("first_name"))
            .lastName(row.getString("last_name"))
            .age(row.getObject("age", Integer.class))
            .build();

    private final JdbcTemplate jdbcTemplate;

    public StudentsRepositoryJdbcTemplateImpl(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    @Override
    public List<Student> findAll() {
        return jdbcTemplate.query(SQL_SELECT_ALL_STUDENTS, studentMapper);
    }

    @Override
    public void save(Student student) {
        Map<String, Object> paramsAsMap = new HashMap<>();

        paramsAsMap.put("first_name", student.getFirstName());
        paramsAsMap.put("last_name", student.getLastName());
        paramsAsMap.put("email", student.getEmail());
        paramsAsMap.put("password", student.getPassword());
        paramsAsMap.put("phone_number", "default");

        SimpleJdbcInsert insert = new SimpleJdbcInsert(jdbcTemplate);

        Long id = insert.withTableName("student")
                .usingGeneratedKeyColumns("id")
                .executeAndReturnKey(new MapSqlParameterSource(paramsAsMap)).longValue();


        student.setId(id);
    }

    @Override
    public Optional<Student> findById(Long id) {
        try {
            return Optional.of(jdbcTemplate.queryForObject(SQL_SELECT_BY_ID, studentMapper, id));
        } catch (EmptyResultDataAccessException e) {
            return Optional.empty();
        }
    }

    @Override
    public void update(Student student) {
        jdbcTemplate.update(SQL_UPDATE_STUDENT_USING_ID,
                student.getFirstName(),
                student.getLastName(),
                student.getAge(),
                student.getEmail(),
                student.getPassword(),
                student.getId());
    }

    @Override
    public void delete(Long id) {
        jdbcTemplate.update(SQL_DELETE_STUDENT, id, id);
    }

    @Override
    public List<Student> findAllByAgeGreaterThanOrderByIdDesc(Integer minAge) {
        List<Map<String, Object>> auxiliaryMap;
        List<Student> students = new ArrayList<>();
        auxiliaryMap = jdbcTemplate.queryForList(SQL_FIND_STUDENTS_WITH_AGE_GREATER_THAN_MINAGE_ORDER_BY_ID_DESC, minAge);
        for(Map<String, Object> map: auxiliaryMap){
            students.add(Student.builder()
                    .id((Long) map.get(("id")))
                    .firstName((String) map.get("first_name"))
                    .lastName((String) map.get("last_name"))
                    .age((Integer) map.get("age"))
                    .email((String) map.get("email"))
                    .password((String) map.get("password"))
                    .build());
        }
        return students;
    }
}
