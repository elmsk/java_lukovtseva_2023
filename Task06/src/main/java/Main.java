import jdbc.SimpleDataSource;
import models.Student;
import repositories.StudentsRepository;
import repositories.StudentsRepositoryJdbcTemplateImpl;
import services.StudentsService;
import services.StudentsServiceImpl;

import javax.sql.DataSource;
import java.io.IOException;
import java.util.Properties;

public class Main {


    public static void main(String[] args) {
        Properties properties = new Properties();
        try {
            properties.load(Main.class.getResourceAsStream("/db.properties"));
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

        DataSource dataSource = new SimpleDataSource(
                properties.getProperty("db.url"),
                properties.getProperty("db.username"),
                properties.getProperty("db.password")
        );

        StudentsRepository studentsRepository = new StudentsRepositoryJdbcTemplateImpl(dataSource);

        Student student = Student.builder()
                .firstName("Hi!")
                .lastName("Bye")
                .email("email")
                .password("qwerty0")
                .build();

        Student student1 = Student.builder()
                .firstName("Марсель")
                .lastName("Сидиков")
                .age(22)
                .id(1L)
                .build();

        studentsRepository.update(student1);

        studentsRepository.delete(1L);

        studentsRepository.save(student);

        System.out.println(studentsRepository.findAllByAgeGreaterThanOrderByIdDesc(22));


    }
}
