package ru.kpfu.itis.repositories;

import ru.kpfu.itis.models.User;

import javax.jws.soap.SOAPBinding;
import java.io.*;
import java.util.ArrayList;
import java.util.List;

import java.util.UUID;
import java.util.function.Function;

/**
 * 30.06.2022
 * 02. TaxiService
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class UsersRepositoryFilesImpl implements UsersRepository {

    private final String fileName;

    private static final Function<User, String> userToString = user ->
            user.getId()
                    + "|" + user.getFirstName()
                    + "|" + user.getLastName()
                    + "|" + user.getEmail()
                    + "|" + user.getPassword();

    public UsersRepositoryFilesImpl(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public List<User> findAll() {
        List<User> allUsers = new ArrayList<>();
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName))){
            String line;
            while((line = bufferedReader.readLine()) != null){
                String[] arrayWithUserInfo = line.split("\\|");
                allUsers.add(new User(UUID.fromString(arrayWithUserInfo[0]),
                        arrayWithUserInfo[1],
                        arrayWithUserInfo[2],
                        arrayWithUserInfo[3],
                        arrayWithUserInfo[4]));
            }
            return allUsers;
        }
        catch (IOException e) {
            throw new IllegalArgumentException();
        }
    }

    @Override
    public void save(User entity) {
        entity.setId(UUID.randomUUID());
        try (Writer writer = new FileWriter(fileName, true);
             BufferedWriter bufferedWriter = new BufferedWriter(writer)) {

            String userAsString = userToString.apply(entity);
            bufferedWriter.write(userAsString);
            bufferedWriter.newLine();

        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void update(User entity) {
        List<User> users = findAll();
        int userIndex = 0;
        boolean ifExist = false;
        for (User user: users){
            if (user.getId().equals(entity.getId())){
                ifExist = true;
                break;
            }
            userIndex++;
        }
        if (ifExist){
            users.set(userIndex, entity);
            rewriteUsersFile(users);
        }
    }

    @Override
    public void delete(User entity) {
        deleteById(entity.getId());
    }

    @Override
    public void deleteById(UUID id) {
        List<User> users = findAll();
        boolean ifExist = false;
        for (User user: users){
            if (user.getId().equals(id)){
                users.remove(user);
                ifExist = true;
                break;
            }
        }
        if (ifExist) {
            rewriteUsersFile(users);
        }
    }

    @Override
    public User findById(UUID uuid) {
        List<User> users = findAll();
        int userIndex = 0;
        boolean ifExist = false;
        for (User user: users){
            if (user.getId().equals(uuid)){
                ifExist = true;
                break;
            }
            userIndex++;
        }
        if (ifExist) {
            return users.get(userIndex);
        }
        return null;
    }

    private void rewriteUsersFile(List<User> users){
        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(fileName, false))){
            for (User user: users) {
                bufferedWriter.write(userToString.apply(user));
                bufferedWriter.newLine();
            }
            bufferedWriter.flush();
        }
        catch (IOException e) {
            throw new IllegalArgumentException();
        }
    }
}
