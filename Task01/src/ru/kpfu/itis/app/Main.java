package ru.kpfu.itis.app;


import ru.kpfu.itis.dto.SignUpForm;
import ru.kpfu.itis.mappers.Mappers;
import ru.kpfu.itis.models.User;
import ru.kpfu.itis.repositories.UsersRepository;
import ru.kpfu.itis.repositories.UsersRepositoryFilesImpl;
import ru.kpfu.itis.services.UsersService;
import ru.kpfu.itis.services.UsersServiceImpl;

import java.util.List;
import java.util.UUID;

public class Main {

    public static void main(String[] args) {
        UsersRepository usersRepository = new UsersRepositoryFilesImpl("users.txt");
        UsersService usersService = new UsersServiceImpl(usersRepository, Mappers::fromSignUpForm);

        usersService.signUp(new SignUpForm("Марсель", "Сидиков",
                "sidikov.marsel@gmail.com", "qwerty007"));
        usersService.signUp(new SignUpForm("Катя" ,"Луковцева",
                "elmsk21@gmail.com", "qwerty008"));
        usersService.signUp(new SignUpForm("Геральт" ,"из Ривии",
                "geralt@mail.com", "qwerty009"));

        List<User> users = usersRepository.findAll();
        System.out.println(users);

        usersRepository.deleteById(users.get(2).getId());

        usersRepository.delete(users.get(0));

        User newUser = new User(users.get(0).getId(), users.get(0).getFirstName(), "НеСидиков", "notsidikov@gmail.com", users.get(0).getPassword());
        usersRepository.update(newUser);

        System.out.println(usersRepository.findById(users.get(0).getId()));

        int i = 0;
    }
}
