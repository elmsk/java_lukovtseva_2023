package ru.kpfu.itis.services;

import ru.kpfu.itis.models.User;
import ru.kpfu.itis.repositories.UsersRepository;
import ru.kpfu.itis.dto.SignUpForm;
import java.util.function.Function;

/**
 * 30.06.2022
 * 02. TaxiService
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class UsersServiceImpl implements UsersService {

    private final UsersRepository usersRepository;

    private final Function<SignUpForm, User> toUserMapper;

    public UsersServiceImpl(UsersRepository usersRepository, Function<SignUpForm, User> toUserMapper) {
        this.usersRepository = usersRepository;
        this.toUserMapper = toUserMapper;
    }

    @Override
    public void signUp(SignUpForm signUpForm) {
        User user = toUserMapper.apply(signUpForm);

        // TODO: сделать проверку, чтобы пользователя с таким Email-не было

        usersRepository.save(user);
    }
}

